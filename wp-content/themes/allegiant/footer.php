			
			<?php if (is_front_page()): ?>
				<?php if ( !cpotheme_show_posts() ) : ?>
					<div class="clear"></div>
					<div id="main" class="main index">
						<div class="container">		
							<section id="content" class="content content-homepage" >
								<?php  
									$wp_query = new WP_Query('showposts=4&order=ASC');
									$i = 0;
								?>
								<?php do_action( 'cpotheme_before_content' ); ?>
								<?php
								if ( have_posts() ) {
									while ( $wp_query->have_posts() ) :
										the_post();
										?>
											<?php if ($i%2 == 0): ?>
												<div class="row">
											<?php endif ?>
													<div class="column column-narrow col2">
														<?php get_template_part( 'template-parts/element', 'blog' ); ?>
													</div>
											<?php if ($i%2 != 0): ?>
												</div>
											<?php endif ?>
											<?php $i++; ?>
										<?php
									endwhile;
								};
								?>
								<?php //cpotheme_numbered_pagination(); ?>
								<?php do_action( 'cpotheme_after_content' ); ?>
							</section>
							<aside id="sidebar" class="sidebar sidebar-primary sidebar-homepage hidden-xs">
								<?php dynamic_sidebar( 'primary-widgets' ); ?>
							</aside>
							<div class="clear"></div>
						</div>
					</div>
				<?php endif; ?>
				<div id="clients" class="clients">
					<div class="container">		
						<div style="margin-bottom:3px;" id="clients-heading" class="clients-heading section-heading">nos Clients</div>
					</div>
					<?php echo do_shortcode('[CPCC id="3806"]'); ?>
				</div>
			<?php endif ?>
			<?php do_action( 'cpotheme_after_main' ); ?>

			<?php get_sidebar( 'footer' ); ?>

			<?php do_action( 'cpotheme_before_footer' ); ?>
			<footer id="footer" class="footer secondary-color-bg dark">
				<div class="container">
					<?php do_action( 'cpotheme_footer' ); ?>
				</div>
			</footer>
			<?php do_action( 'cpotheme_after_footer' ); ?>

			<div class="clear"></div>
		</div><!-- wrapper -->
		<?php do_action( 'cpotheme_after_wrapper' ); ?>
	</div><!-- outer -->
	
	<!-- btn to cintact page -->
	<a href="/contact" style="background-color: rgb(101, 158, 86); margin-top: -34.5px; right: 38px; opacity: 0.96; top: 50%; -moz-transform-origin: right top; -o-transform-origin: right top; transform-origin: right top; -ms-transform-origin: right top; -webkit-transform: rotate(-90deg); -moz-transform: rotate(-90deg); -o-transform: rotate(-90deg); -ms-transform: rotate(-90deg); position: fixed; z-index: 9997; text-decoration: none; color: white; -webkit-border-radius: 4px; -moz-border-radius: 4px; -o-border-radius: 4px; -ms-border-radius: 4px; padding: 14px 15px 12px 12px; line-height: 14px; float: none; text-shadow: none;">
	    <i class="fa fa-envelope"></i> Contact
	</a>
	<a href="/demande-demo/" style="background-color: rgb(134, 138, 142); margin-top: -34.5px; right: 38px; opacity: 0.96; top: 70%; -moz-transform-origin: right top; -o-transform-origin: right top; transform-origin: right top; -ms-transform-origin: right top; -webkit-transform: rotate(-90deg); -moz-transform: rotate(-90deg); -o-transform: rotate(-90deg); -ms-transform: rotate(-90deg); position: fixed; z-index: 9997; text-decoration: none; color: white; -webkit-border-radius: 4px; -moz-border-radius: 4px; -o-border-radius: 4px; -ms-border-radius: 4px; padding: 14px 15px 12px 12px; line-height: 14px; float: none; text-shadow: none;">
	    <i class="fa fa-check"></i> Vous souhaitez démo ?
	</a>
	
	<?php wp_footer(); ?>
	<script>
		window.onload = function () {
			window.onscroll = function () {
			    if (window.pageYOffset > 120) {
			      	document.getElementById('header').classList.add("fixed");
			    } else {
			    	document.getElementById('header').classList.remove("fixed");
			    }
			}
		}
	</script>
</body>
</html>


