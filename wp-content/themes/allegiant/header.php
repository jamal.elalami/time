<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<div class="outer" id="top">
		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas nostrum voluptatum ex, laboriosam accusamus quaerat accusantium numquam expedita nihil quis maxime, velit temporibus, minus earum, laborum error aspernatur aliquam et!
		<?php do_action( 'cpotheme_before_wrapper' ); ?>
		<div class="wrapper">
			<div id="topbar" class="topbar">
				<div class="container">
					<?php do_action( 'cpotheme_top' ); ?>
					<div class="clear"></div>
				</div>
			</div>
			<header id="header" class="header">
				<div class="container">
					<?php do_action( 'cpotheme_header' ); ?>
					<div class='clear'></div>
				</div>
			</header>
			<?php do_action( 'cpotheme_before_main' ); ?>
			<div class="clear"></div>
